﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace Domain
{
    public class MostProblematicSelector : IWordSelectorForRevision
    {
        private IWordsRepository wordsRepo;
        private IShotsRepository shotsRepo;

        public MostProblematicSelector(IWordsRepository wordRepo, IShotsRepository shotRepo)
        {
            this.wordsRepo = wordRepo;
            this.shotsRepo = shotRepo;
        }
        public IEnumerable<Word> GetWordsForRevision()
        {
            //OrderBy is stable
            IOrderedEnumerable<Word> byDate = wordsRepo.FindAll().OrderBy(word => word.DailyAge());
            Dictionary<Word, double> wordAccuracy = new Dictionary<Word, double>();
            foreach (Word word in byDate)
            {
                wordAccuracy.Add(word,  word.PassedPercentage(shotsRepo));
            }

            IOrderedEnumerable<Word> byAccuracy = byDate.OrderBy(word => wordAccuracy[word]);

            return byAccuracy.Take(10);
        }
    }
}
