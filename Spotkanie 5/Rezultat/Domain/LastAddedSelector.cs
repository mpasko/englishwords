﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace Domain
{
    public class LastAddedSelector : IWordSelectorForRevision
    {
        private IWordsRepository wordsRepo;
        private IShotsRepository shotsRepo;

        public LastAddedSelector(IWordsRepository wordsRepository, IShotsRepository shotRepo)
        {
            this.wordsRepo = wordsRepository;
            this.shotsRepo = shotRepo;
        }

        public IEnumerable<Word> GetWordsForRevision()
        {
            IEnumerable<Word> all = wordsRepo.FindAll();
            IEnumerable<Word> thisWeek = all.Where(word => word.DailyAge() <= 7);
            if (thisWeek.Count() == 0)
            {
                throw new Exception("Lazy User");
            }
            Func<Word, bool> negativeSelector = word => ! shotsRepo.FindByWordId(word.Id).Any(shot => shot.Result == true);
            return thisWeek.Where(negativeSelector).Take(10);
        }
    }
}
