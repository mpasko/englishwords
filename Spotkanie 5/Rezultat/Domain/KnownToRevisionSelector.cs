﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace Domain
{
    public class KnownToRevisionSelector : IWordSelectorForRevision
    {
        private IWordsRepository wordsRepo;
        private IShotsRepository shotsRepo;

        KnownToRevisionSelector(IWordsRepository wordRepo, IShotsRepository shotRepo)
        {
            this.wordsRepo = wordRepo;
            this.shotsRepo = shotRepo;
        }

        public IEnumerable<DataAccess.Word> GetWordsForRevision()
        {
            return wordsRepo.FindAll().Where(word => CountAccuracy(word) < 0.95).Take(10);
        }

        private double CountAccuracy(Word word)
        {
            IEnumerable<Shot> wordShots = shotsRepo.FindByWordId(word.Id);
            Func<Shot, int> weekPeriodSelector = shot => DateTime.Now.Subtract(shot.Date).Days;
            Shot last = wordShots.OrderBy(weekPeriodSelector).First();
            IEnumerable<Shot> scope = shotsRepo.FindAll().Where(shot => last.Date.Subtract(shot.Date).Days > 7);
            double passed = scope.Count(shot => shot.Result == true);
            return passed / scope.Count();
        }
    }
}
