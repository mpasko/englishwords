﻿using System;
using System.Collections.Generic;
using DataAccess;
using System.Linq;

namespace Domain
{
	public class Top10WordsSelector : IWordSelectorForRevision
	{
		private IWordsRepository wordsRepository;

		public Top10WordsSelector(IWordsRepository wordsRepository)
		{
			this.wordsRepository = wordsRepository;
		}


		public IEnumerable<Word> GetWordsForRevision()
		{
			if (wordsRepository.FindAll().Count()<10)
				throw new Exception("not enough words");

			return wordsRepository.FindAll().Take(10);
		}
	}
}
