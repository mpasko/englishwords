﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace Domain
{
    public class MixedRevisionSelector : IWordSelectorForRevision
    {
        private IWordSelectorForRevision knownToRevision;
        private IWordSelectorForRevision mostProblematic;
        private IWordSelectorForRevision lastAdded;

        public MixedRevisionSelector(IWordSelectorForRevision lastAddedSelector, 
            IWordSelectorForRevision mostProblematicSelector, 
            IWordSelectorForRevision knownToRevisionsSelector)
        {
            this.lastAdded = lastAddedSelector;
            this.mostProblematic = mostProblematicSelector;
            this.knownToRevision = knownToRevisionsSelector;
        }

        public IEnumerable<DataAccess.Word> GetWordsForRevision()
        {
            int total = 0;
            IEnumerable<Word> last = lastAdded.GetWordsForRevision().Take(7);
            total += last.Count();
            IEnumerable<Word> problematic = mostProblematic.GetWordsForRevision().Take(14 - total);
            total += problematic.Count();
            IEnumerable<Word> known = knownToRevision.GetWordsForRevision().Take(20 - total);
            return last.Concat(problematic).Concat(known);
        }
    }
}
