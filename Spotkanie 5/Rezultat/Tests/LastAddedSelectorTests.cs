﻿using System;
using System.Collections.Generic;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using Tests.Utils;

namespace Tests
{
    [TestClass]
    public class LastAddedSelectorTests
    {
        private Mock<IWordsRepository> wordsrepo;
        private Mock<IShotsRepository> shotsrepo;

        [TestMethod]
        public void To_take()
        {
            wordsrepo = new Mock<IWordsRepository>();
            shotsrepo = new Mock<IShotsRepository>();
            //given
            WordFactory wordFactory = new WordFactory(wordsrepo);
            ShotFactory shotFactory = new ShotFactory(shotsrepo);
            List<Word> toSkip = new List<Word>();
            for (int i = 0; i < 5; ++i)
            {
                Word word = wordFactory.New().Word;
                toSkip.Add(word);
                shotFactory.New().WithResult(true).Associate(word);
            }      
            for (int i = 0; i < 5; ++i)
            {
                Word word = wordFactory.New().At(8+i).Word;
                toSkip.Add(word);
                shotFactory.New().WithResult(false).Associate(word);
            }
            List<Word> toPass = new List<Word>();
            for (int i = 0; i < 5; ++i)
            {
                toPass.Add(wordFactory.New().Word);
            }
            //wordsrepo.Setup(x => x.FindAll()).Returns(new List<Word>(toPass).Concat(toSkip));
            var wordselector = new LastAddedSelector(wordsrepo.Object, shotsrepo.Object);
            //when
            var words = wordselector.GetWordsForRevision();
            //then
            foreach (Word pass in toPass)
            {
                Assert.IsTrue(words.Contains(pass));
            }
            foreach (Word skip in toSkip)
            {
                Assert.IsFalse(words.Contains(skip));
            }
        }

        [TestMethod]
        public void take_10()
        {
            wordsrepo = new Mock<IWordsRepository>();
            shotsrepo = new Mock<IShotsRepository>();
            //given
            WordFactory wordFactory = new WordFactory(wordsrepo);
            ShotFactory shotFactory = new ShotFactory(shotsrepo);
            
            List<Word> toPass = new List<Word>();
            for (int i = 0; i < 15; ++i)
            {
                toPass.Add(wordFactory.New().Word);
            }
            //wordsrepo.Setup(x => x.FindAll()).Returns(toPass);
            var wordselector = new LastAddedSelector(wordsrepo.Object, shotsrepo.Object);
            //when
            var words = wordselector.GetWordsForRevision();
            //then
            Assert.AreEqual(10,words.Count());
        }

        [TestMethod]
        public void Throws_lazyUser()
        {
            wordsrepo = new Mock<IWordsRepository>();
            shotsrepo = new Mock<IShotsRepository>();
            //given
            //shotsrepo.Setup(x => x.FindAll()).Returns(new List<Shot> {
                //empty
            //});
            var wordselector = new LastAddedSelector(wordsrepo.Object, shotsrepo.Object);
            
            //when
            try
            {
                var words = wordselector.GetWordsForRevision();
                Assert.Fail("exception expected");
            }
            catch (Exception)
            {
                Assert.IsFalse(false);
            }
        }
    }
}
