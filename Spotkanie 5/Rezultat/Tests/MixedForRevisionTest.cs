﻿using System;
using System.Collections.Generic;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace Tests
{
    [TestClass]
    public class MixedForRevisionTest
    {
        private Mock<IWordSelectorForRevision> knownToRevision;
        private Mock<IWordSelectorForRevision> mostProblematic;
        private Mock<IWordSelectorForRevision> lastAdded;

        
        [TestMethod]
        public void GetWordsForRevision_findall_called()
        {
            knownToRevision = new Mock<IWordSelectorForRevision>();
            mostProblematic = new Mock<IWordSelectorForRevision>();
            lastAdded = new Mock<IWordSelectorForRevision>();
            List<Word> words = new List<Word> { new Word() };
            knownToRevision.Setup(x => x.GetWordsForRevision()).Returns(words);
            mostProblematic.Setup(x => x.GetWordsForRevision()).Returns(words);
            lastAdded.Setup(x => x.GetWordsForRevision()).Returns(words);

            IWordSelectorForRevision mixed = new MixedRevisionSelector(lastAdded.Object, mostProblematic.Object, knownToRevision.Object);

            IEnumerable<Word> result = mixed.GetWordsForRevision();

            knownToRevision.Verify(x => x.GetWordsForRevision());
            mostProblematic.Verify(x => x.GetWordsForRevision());
            lastAdded.Verify(x => x.GetWordsForRevision());
            Assert.AreEqual(3, result.Count());
        }
    }
}
