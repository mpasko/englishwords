﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tests.Utils;

namespace Tests
{
    [TestClass]
    public class MostProblematicSelectorTest
    {
        private Mock<IWordsRepository> wordsrepo;
        private Mock<IShotsRepository> shotsrepo;
        private WordFactory wordfactory;

        [TestMethod]
        public void GetWordsForRevision_top_ten_select()
        {
            wordsrepo = new Mock<IWordsRepository>();
            shotsrepo = new Mock<IShotsRepository>();
            wordfactory = new WordFactory(wordsrepo);
            ShotSmartMock shotSmartMock;
            Word word;
            double accuracy;
            ISet<int> itemsToChoose = new SortedSet<int>{2,5,6,8,10,13,14,15,18,19};
            for (int i = 0; i < 20; ++i)
            {
                word = wordfactory.New().Word;
                if (itemsToChoose.Contains(i)) { accuracy = 0.1; }
                else { accuracy = 0.9; }
                shotSmartMock = new ShotSmartMock(word, accuracy);
                shotsrepo.Setup(x => x.FindByWordId(word.Id)).Returns(shotSmartMock);
                word.Id = i;
            }
            IWordSelectorForRevision selector = new MostProblematicSelector(wordsrepo.Object,shotsrepo.Object);
            IEnumerable<Word> wordsToRevision = selector.GetWordsForRevision();

            Assert.AreEqual(10, wordsToRevision.Count());
            foreach (Word w in wordsToRevision)
            {
                Assert.IsTrue(itemsToChoose.Contains(w.Id));
            }
        }
    }
}
