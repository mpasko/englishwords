﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace Tests.Utils
{
    class ShotSmartMock : IEnumerable<Shot>
    {
        int iteration;
        int totall;
        private double accuracy;
        private Word word;
        List<Shot> shots;

        public ShotSmartMock(Word word, double accuracy)
        {
            totall = 50;
            iteration = 0;
            this.word = word;
            this.accuracy = accuracy;

            shots = new List<Shot>();
            for (iteration = 0; iteration < totall; ++iteration)
            {
                Shot shot = new Shot();
                shot.Id = iteration;
                shot.Result = (double)iteration / (double)totall >= 1-accuracy;
                shot.Date = DateTime.Now;
                shot.Word = word;
                shots.Add(shot);
            }
        }

        public IEnumerator<Shot> GetEnumerator() { return shots.GetEnumerator(); }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator(){ return GetEnumerator();  }
    }
}
