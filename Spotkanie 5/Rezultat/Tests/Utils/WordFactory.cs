﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace Tests.Utils
{
    class WordFactory
    {
        private Word word;
        private static int id_gen = 0;
        private Moq.Mock<IWordsRepository> wordsrepo;

        LinkedList<Word> All { get; set; }

        public WordFactory(Moq.Mock<IWordsRepository> wordsrepo)
        {
            All = new LinkedList<Word>();
            this.wordsrepo = wordsrepo;
            resetup();
        }

        public WordFactory New()
        {
            this.word = new Word();
            this.word.Id = id_gen++;
            this.word.Added = DateTime.Now;
            this.All.AddLast(word);
            resetup();
            return this;
        }

        public Word Word
        {
            get
            {
                return word;
            }
        }

        public WordFactory At(int seq)
        {
            word.Added = DateTime.Now.AddDays(-seq);
            return this;
        }

        private void resetup()
        {
            wordsrepo.Setup(x => x.FindAll()).Returns(All);
        }
    }
}
