﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using Moq;

namespace Tests.Utils
{
    class ShotFactory
    {
        private Mock<IShotsRepository> mock;
        private Shot shot;

        public ShotFactory(Mock<IShotsRepository> mock)
        {
            this.mock = mock;
        }

        public ShotFactory New()
        {
            this.shot = new Shot();
            return this;
        }

        public ShotFactory WithResult(bool result)
        {
            shot.Result = result;
            return this;
        }

        public ShotFactory Associate(Word word)
        {
            shot.Word = word;
            List<Shot> current = new List<Shot>(mock.Object.FindByWordId(word.Id));
            List<Shot> all = new List<Shot>(mock.Object.FindAll());
            current.Add(shot);
            all.Add(shot);
            mock.Setup(x => x.FindByWordId(word.Id)).Returns(current);
            mock.Setup(x => x.FindAll()).Returns(all);
            return this;
        }
    }
}
