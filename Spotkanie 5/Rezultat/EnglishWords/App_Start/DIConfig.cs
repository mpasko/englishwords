﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using DataAccess;


namespace EnglishWords
{
	public class DIConfig
	{
		public static void ConfigureDI()
		{
			var builder = new ContainerBuilder();
			builder.RegisterControllers(typeof(DIConfig).Assembly);

			builder.RegisterModule<DataAccessModule>();

			var container = builder.Build();
			DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
		}
	}
}