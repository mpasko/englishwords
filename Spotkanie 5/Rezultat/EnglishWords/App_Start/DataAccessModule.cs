﻿using Autofac;
using DataAccess;

namespace EnglishWords
{
	public class DataAccessModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<WordsRepository>().As<IWordsRepository>();
			builder.RegisterType<ShotsRepository>().As<IShotsRepository>();
		}
	}
}