﻿
using System;
using System.Collections.Generic;
using System.Linq;
namespace DataAccess
{
    public class Word
    {
	    public int Id { get; set; }
		public string EnglishWord { get; set; }
		public string Translation { get; set; }
        public System.DateTime Added { get; set; }

        public double PassedPercentage(IShotsRepository shotsRepo)
        {
            IEnumerable<Shot> shots = shotsRepo.FindByWordId(Id);
            int possitive = shots.Where(shot => shot.Result==true).Count();
            return (double)possitive / (double)shots.Count();
        }

        public int DailyAge()
        {
            return DateTime.Now.Subtract(Added).Days;
        }
    }
}
