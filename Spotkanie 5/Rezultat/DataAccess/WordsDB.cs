﻿using System.Data.Entity;

namespace DataAccess
{
	public class WordsDB : DbContext
	{
		public WordsDB()
            : base("Data Source=(local);Initial Catalog=englishwordsdb1;Integrated Security=True")
		{
			
		}

		public DbSet<Word> Words { get; set; }
		public DbSet<Shot> Shots { get; set; }
	}
}
