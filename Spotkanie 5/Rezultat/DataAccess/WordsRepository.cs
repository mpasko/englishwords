﻿
using System;
using System.Collections.Generic;

namespace DataAccess
{
	public class WordsRepository : Repository, IWordsRepository
	{
		public void Add(Word word)
		{
            word.Added = DateTime.Now;
			context.Words.Add(word);
			context.SaveChanges();
		}

		public IEnumerable<Word> FindAll()
		{
			return context.Words;
		}
	}
}
