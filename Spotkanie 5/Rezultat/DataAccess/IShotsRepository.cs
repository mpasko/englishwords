using System.Collections.Generic;

namespace DataAccess
{
	public interface IShotsRepository
	{
		IEnumerable<Shot> FindAll();
        IEnumerable<Shot> FindByWordId(int id);
	}
}