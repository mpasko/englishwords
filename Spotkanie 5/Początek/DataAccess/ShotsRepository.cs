﻿using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
	public class ShotsRepository  : Repository, IShotsRepository
	{
		public IEnumerable<Shot> FindAll()
		{
			return context.Shots.ToList();

			
		}

		public IEnumerable<Shot> FindByWordId(int id)
		{
			return context.Shots.Where(s => s.Word.Id == id);

		}
	}
}
