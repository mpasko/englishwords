﻿
namespace DataAccess
{
    public class Word
    {
	    public int Id { get; set; }
		public string EnglishWord { get; set; }
		public string Translation { get; set; }
    }
}
